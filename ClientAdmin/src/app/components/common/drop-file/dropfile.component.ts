import { OssImageService } from '../../../services/common/oss-image.service';
import { AuthenticateService } from '../../../services/authenticate.service';

import { Component, OnInit, NgZone, ViewChild, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
declare var $: any;
declare var swal: any;

@Component({
    selector: 'drop-file',
    templateUrl: 'dropfile.component.html'
})

export class DropfileComponent implements OnInit {
    public hiddenFileInput: HTMLInputElement;
    public dropFile: Element;
    public files: any[] = [];
    @Input() destDir: string;
    @Input() numberOfFiles: number = 1;
    @Input() fileSizeLimit: number = 6000000;
    @Output() onSaveFiles = new EventEmitter<string[]>();
    @Output() onFilesChangedEvent = new EventEmitter<string[]>();
    @Output() onUploaded: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onBeginUpload: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(private authenticateService: AuthenticateService,
        private ossService: OssImageService, private zone: NgZone,
        private ref: ChangeDetectorRef) {

    }
    ngOnInit(): void {
        var self = this;
        this.setupHiddenFileInput();
        this.dropFile = document.getElementsByClassName('dropfile')[0];
        this.dropFile.addEventListener('dragover', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });
        this.dropFile.addEventListener('dragleave', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });
        this.dropFile.addEventListener('drop', function (e: any) {
            e.preventDefault();
            e.stopPropagation();
            var dataTransfer = e.dataTransfer;
            console.log(dataTransfer);
            if (dataTransfer && dataTransfer.files) {
                if (dataTransfer.files.length > 0) {
                    console.log(dataTransfer.files[0]);
                    var file = dataTransfer.files[0];
                    if (file) {
                        var fileExtension = file.name.split('.').pop().toLowerCase();
                        if(self.validateFileExtension(fileExtension)){
                            var fr = new FileReader();
                            fr.addEventListener("loadend", function () {
                                var anyFile = {
                                    file: this.result,
                                    name: file.name,
                                    origin_file: file
                                };
                                if(self.files.length < 1){
                                    self.files.push(anyFile);
                                    self.save(anyFile);
                                }
                                else{
                                    if(self.files[0].name != anyFile.name){
                                        var fileUpload = [];
                                        fileUpload.push(anyFile);
                                        self.deleteFileUpload(event, 0, fileUpload);
                                        swal('', 'upload error', 'error');
                                    }
                                }
                            });
                            fr.readAsDataURL(file);
                        }
                        else{
                            swal('', 'upload error', 'error');
                        }
                    }
                }
            }
        });

        this.dropFile.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (self.hiddenFileInput) {
                self.hiddenFileInput.click();
            }
        });
    }
    validateFileExtension(word): boolean {
        var allowedExtensions = ["jpg","jpeg","png","bmp","gif","pdf"];
        return allowedExtensions.indexOf(word.toLowerCase()) > -1;
    }
    public resetFileUpload(): void {
        this.setupHiddenFileInput();
    }
    private setupHiddenFileInput(): void {
        var self = this;
        this.hiddenFileInput = document.createElement("input");
        this.hiddenFileInput.setAttribute("type", "file");
        this.hiddenFileInput.style.visibility = "hidden";
        this.hiddenFileInput.style.position = "absolute";
        this.hiddenFileInput.style.top = "0";
        this.hiddenFileInput.style.left = "0";
        this.hiddenFileInput.style.height = "0";
        this.hiddenFileInput.style.width = "0";
        this.hiddenFileInput.addEventListener('change', function (e) {
            var files = self.hiddenFileInput.files;
            if (files && files.length > 0) {
                var file = files[0];
                var fr = new FileReader();
                fr.addEventListener("loadend", function () {
                    var anyFile = { file: this.result, name: file.name, origin_file: file };
                    var fileExtension = file.name.split('.').pop().toLowerCase();
                    if(self.validateFileExtension(fileExtension)){
                        if(self.files.length < 1){
                            self.files.push(anyFile);
                            self.save(anyFile);
                        }
                        else{
                            if(self.files[0].name != anyFile.name){
                                var fileUpload = [];
                                fileUpload.push(anyFile);
                                self.deleteFileUpload(event, 0, fileUpload);
                                swal('', 'upload error', 'error');
                            }
                        }
                    }
                    else{
                        swal('', 'upload error', 'error');
                    }
                });
                fr.readAsDataURL(file);
            }  
        });
        document.body.appendChild(this.hiddenFileInput);
    }

    private deleteFileUpload(e, index, files): void {
        this.resetFileUpload();
        console.log('deleteFileUpload', e);
        e.preventDefault();
        e.stopPropagation();
        if (files) {
            files.splice(index, 1);
            this.onFilesChangedEvent.emit([]);
        }
    }

    generateFileName(file: File) {
        var date = new Date();
        var sufix = date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();
        var fileName = file.name.split('.')[0];
        var fileExtension = file.name.split('.').pop();
        fileName = fileName.replace(/[ ()$%^!@#&+~]+/g,'-');
        return fileName + '_' + sufix + '.' + fileExtension;
    }

    private save(file: any): void {
        var self = this;
        this.ossService.getkey().then(result => {
            if (result.success) {
                var data = result.data;
                var promiseAll = [];
                console.log(file);
                if (file != undefined && file != null) {
                    var fileName = self.destDir + "/" + self.generateFileName(file.origin_file);
                    self.onBeginUpload.emit(true);
                    var promise = this.ossService.singleUploadToOss(file.origin_file, fileName, data.url, data.key, data.secret, function (progress) {
                        self.zone.run(() => {
                            file.progress = progress;
                            self.ref.detectChanges();
                        });
                    });
                    promise.then(function (addOk) {
                        var ss = [data.url + '/' + fileName];
                        self.onUploaded.emit(true);
                        self.onSaveFiles.emit(ss);
                    });
                }
            }
        });
    }

    private deleteFile(e, index): void {
        this.resetFileUpload();
        console.log('deleteFile', e);
        e.preventDefault();
        e.stopPropagation();
        if (this.files) {
            this.files.splice(index, 1);
            this.onFilesChangedEvent.emit([]);
        }
    }

}