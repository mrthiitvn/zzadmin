import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ApiService} from '../services/api.service';
import {UserModel} from '../model/user.model';
import { ApiResult } from '../model/api.result.model';
@Injectable()
export class RoleService {
    constructor(private _apiService: ApiService) {

    }
    GetAll(): Promise<ApiResult> {
        let url = `${environment.baseApiUrl}/Role/GetAll`;
        return this._apiService.httpGet(url, true);
    }
    CreateUser(role: any): Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/Role/Create`;
        return this._apiService.httpPost(url, role, false);
    }
}