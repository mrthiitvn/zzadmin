import { ApiResult } from '../model/api.result.model';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ConfigType } from '../model/config.type.model';
import { environment } from '../../environments/environment';

@Injectable()
export class ConfigTypeService {

    //public _apiService: ApiService;
    constructor(public apiService: ApiService){
        //this._apiService = apiService;
     };

     getConfigType() : Promise<ApiResult>{
         const url = `${environment.baseApiUrl}/ConfigType/GetAll`;
         return this.apiService.httpGet(url, false);
     }; 

     getConfigTypeById(id) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/ConfigType/GetById?Id=${id}`;
        return this.apiService.httpGet(url, false);
    }; 

    createConfigType(model) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/ConfigType/Create`;
        return this.apiService.httpPost(url, model, false);
    }; 

    updateConfigType(model) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/ConfigType/Update`;
        return this.apiService.httpPut(url, model, false);
    }; 

     deleteConfigType(object) : Promise<ApiResult> {
         const url = `${environment.baseApiUrl}/ConfigType/Delete/${object.Id}`;
         return this.apiService.httpPut(url, object);
     };


     changeConfigType(object) : Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/ConfigType/ChangeStatus/${object.Id}`;
        return this.apiService.httpPut(url, object);
    };
}
