import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SessionData } from '../model/session.data.model';

@Injectable()
export class SocketService {
    public socket = new BehaviorSubject<any>(null);
}