import { Component, ViewChild, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2'
import { UserModel } from '../../model/user.model';
import { SessionData } from '../../model/session.data.model';
import { removeSummaryDuplicates } from '@angular/compiler';
@Component({
  selector: 'detail-user',
  templateUrl: './user.detail.component.html'
})
export class UserDetailComponent implements OnInit {
  public title: string = "Cao Hoang Chuyen";
  public totalItem: number;
  public modelUser: any = new UserModel();
  public seletedUser: any;
  public userId: string;
  public session: SessionData;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService ) { }

  ngOnInit() {
    this.GetUser();
  }

  GetUser(): void {debugger;
    this.route.params.subscribe(params => {
      this.userId = params["id"];
      // if (!this.session || this.session.Id == this.userId) {
      //   this.router.navigate(['/user']);
      //   return;
      // }
      // else{
        this.userService.GetUser(this.userId).then(result => {
          if(result != null && result.success){
            if(result.data != null){
              this.modelUser = result.data;
            }
            else{
              swal('', 'Lấy thông tin người dùng thất bại', 'error');
            }
          }
        }).catch(error => {
          swal('', error, 'error')
        });
      // }
    });
  }
  OnFilesChangedEvent(event): void {
  }

  OnSaveImagesHandler(files: File[]): void {
    console.log(files);
  }

  onImageUploaded(event): void {
    // this.uploadingImageModal.hide();
  }
  
  onImageBeginUpload(event): void {
  }

  onSaveFilesHandler(files): void {
    this.modelUser.pictureUrl = files[0];
  }

  OnDeleteFileHandler(): void{
    this.modelUser.pictureUrl = '';
  }

  Save(): void {
    debugger;
    if(this.userId == '0'){
      this.userService.CreateUser(this.modelUser).then(result => {
        this.modelUser = new UserModel();
        swal('', result.message, 'success');
      });
    }
    else{
      this.userService.UpdateUser(this.modelUser).then(result => {
        swal('', result.message, 'success');
      });
    }
  }
}
